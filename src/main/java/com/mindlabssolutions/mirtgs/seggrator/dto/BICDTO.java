package com.mindlabssolutions.mirtgs.seggrator.dto;

import lombok.Data;

@Data
public class BICDTO {

    private Long id;
    private String mxCode;
    private String participantCode;
    private Boolean active;
}
