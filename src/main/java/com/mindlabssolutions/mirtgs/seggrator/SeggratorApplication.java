package com.mindlabssolutions.mirtgs.seggrator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeggratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeggratorApplication.class, args);
	}

}
