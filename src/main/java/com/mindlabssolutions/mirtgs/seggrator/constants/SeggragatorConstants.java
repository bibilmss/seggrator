package com.mindlabssolutions.mirtgs.seggrator.constants;

public class SeggragatorConstants {
    public final  static  String STP_SUPPORTED_MESSAGES_QUEUE="STP_SUPPORTED_MESSAGES_QUEUE";
    public final static String STP_OPPOSED_MESSAGES_QUEUE ="STP_OPPOSED_MESSAGES_QUEUE";
}
