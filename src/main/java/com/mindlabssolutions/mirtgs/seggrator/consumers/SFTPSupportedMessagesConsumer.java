package com.mindlabssolutions.mirtgs.seggrator.consumers;

import ch.qos.logback.classic.Logger;
import com.google.gson.Gson;
import com.mindlabssolutions.mirtgs.seggrator.constants.SeggragatorConstants;
import com.mindlabssolutions.mirtgs.seggrator.pojos.SFTPSwiftFile;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;

@Component
public class SFTPSupportedMessagesConsumer {
    private Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(this.getClass());

    @Value("${input-folder-integration}")
    private String inputDirectory;

    @JmsListener(destination = SeggragatorConstants.STP_SUPPORTED_MESSAGES_QUEUE)
    public void receive(Message message)
    {
        Gson gson = new Gson();

        if(message.getPayload() instanceof String)
        {

            String json =(String)	message.getPayload();
            SFTPSwiftFile ftpSwiftFile = gson.fromJson(json,SFTPSwiftFile.class);
            logger.info("supported message:{}",ftpSwiftFile.getFilename());


            try {
                FileOutputStream fileOutputStream = new FileOutputStream(inputDirectory + ftpSwiftFile.getFilename());
                fileOutputStream.write(ftpSwiftFile.getContent().getBytes());
                fileOutputStream.close();


            }catch (Exception e){
                logger.error("",e);
            }




        }
    }
}
