package com.mindlabssolutions.mirtgs.seggrator.pojos;

import lombok.Data;

@Data
public class SFTPSwiftFile {
    private String filename;
    private String content;
}
