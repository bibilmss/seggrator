package com.mindlabssolutions.mirtgs.seggrator.dao;

import com.mindlabssolutions.mirtgs.seggrator.dto.BICDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BICRepository {

    @Autowired
//    @Qualifier("mirtgsJDBCTemplate")
    private JdbcTemplate jdbcTemplate;



    public List<BICDTO> findByMxCodeAndActiveTrue(String mxCode)
    {
       return jdbcTemplate.query("select * from bic_master bc where bc.mx_code= ? and bc.active = 1",new Object[]{mxCode},new BICRowMapper());
    }


}
