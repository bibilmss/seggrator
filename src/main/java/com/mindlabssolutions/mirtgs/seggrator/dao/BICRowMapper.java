package com.mindlabssolutions.mirtgs.seggrator.dao;

import com.mindlabssolutions.mirtgs.seggrator.dto.BICDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;


public class BICRowMapper implements RowMapper<BICDTO> {




    @Override
    public BICDTO mapRow(ResultSet resultSet, int i) throws SQLException {

        BICDTO bicdto = new BICDTO();

        while (resultSet.next())
        {
            String mxCode = resultSet.getString("mx_code");
            bicdto.setMxCode(mxCode);

            String participantCode = resultSet.getString("participant_code");
            bicdto.setParticipantCode(participantCode);


            Long id = resultSet.getLong("id");
            bicdto.setId(id);

            Boolean active = resultSet.getBoolean("active");
            bicdto.setActive(active);
        }

        return bicdto;
    }
}
