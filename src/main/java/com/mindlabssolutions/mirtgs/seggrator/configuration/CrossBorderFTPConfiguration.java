package com.mindlabssolutions.mirtgs.seggrator.configuration;

import com.jcraft.jsch.ChannelSftp;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.ftp.outbound.FtpMessageHandler;
import org.springframework.integration.ftp.session.DefaultFtpSessionFactory;
import org.springframework.integration.sftp.outbound.SftpMessageHandler;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.messaging.MessageHandler;

@Configuration
@EnableIntegration
public class CrossBorderFTPConfiguration
{


    @Value(value = "${cross-border-host}")
    private String crossBorderHost;

    @Value(value = "${cross-border-username}")
    private String crossBorderUserName;
    @Value(value = "${cross-border-password}")
    private String crossBorderPassword;

    @Value(value = "${cross-border-path}")
    private String crossBorderPath;





    public SessionFactory<FTPFile> ftpSessionFactory()
    {
        DefaultFtpSessionFactory factory = new DefaultFtpSessionFactory();
        factory.setHost(crossBorderHost);//10.200.201.20
        factory.setUsername(crossBorderUserName); //
        factory.setPassword(crossBorderPassword);//
        factory.setClientMode(2);//Always Keep This is as two
        return new CachingSessionFactory<>(factory);
    }


    @Bean
    @ServiceActivator(inputChannel = "CrossBorderSWIFTChannel") MessageHandler uploadHandler()
    {
        FtpMessageHandler messageHandler = new FtpMessageHandler(ftpSessionFactory());
        messageHandler.setRemoteDirectoryExpression(new LiteralExpression(crossBorderPath));


        return messageHandler;
    }

}
