package com.mindlabssolutions.mirtgs.seggrator.configuration;

import com.jcraft.jsch.ChannelSftp;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.sftp.outbound.SftpMessageHandler;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.messaging.MessageHandler;

@Configuration
@EnableIntegration
public class IncomingSFTPConfiguration
{
    @Value(value = "${incoming-host}")
    private String incomingHost;
    @Value(value = "${incoming-username}")
    private String incomingUsername;

    @Value(value = "${incoming-password}")
    private String incomingPassword;


    @Value(value = "${incoming-path}")
    private String incomingPath;




    public SessionFactory<ChannelSftp.LsEntry> ftpSessionFactory()
    {

        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
        factory.setHost(incomingHost);
        factory.setPort(22);
        factory.setUser(incomingUsername);
        factory.setPassword(incomingPassword);
        factory.setAllowUnknownKeys(true);
        return new CachingSessionFactory<>(factory);
    }

    @Bean
    @ServiceActivator(inputChannel = "IncomingFTPChannel")
    public MessageHandler incomingUploadHandler()
    {
        SftpMessageHandler messageHandler = new SftpMessageHandler(ftpSessionFactory());
        messageHandler.setRemoteDirectoryExpression(new LiteralExpression(incomingPath+"/ready"));
        return messageHandler;
    }
}
