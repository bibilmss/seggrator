package com.mindlabssolutions.mirtgs.seggrator.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.boot.spi.EntityManagerFactoryBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManager;
import javax.sql.DataSource;

//@Configuration
public class DSSecondaryConfiguration
{

//    @Bean
//    @ConfigurationProperties("app.datasource.mirtgs")
//    public DataSourceProperties memberDataSourceProperties() {
//        return new DataSourceProperties();
//    }
//
//    @Bean
//    @ConfigurationProperties("app.datasource.mirtgs.configuration")
//    public DataSource mirtgsDataSource() {
//        return memberDataSourceProperties().initializeDataSourceBuilder()
//                .type(HikariDataSource.class).build();
//    }
//
//
//    @Bean("mirtgsJDBCTemplate")
//    public JdbcTemplate mirtgsJDBCTemplate(@Qualifier("mirtgsDataSource") DataSource ccbsDataSource) {
//        return new JdbcTemplate(ccbsDataSource);
//    }

}
