package com.mindlabssolutions.mirtgs.seggrator.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.endpoint.AbstractMessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.messaging.Message;

import java.io.File;
import java.nio.file.Paths;

@EnableIntegration
@Configuration
public class IncomingFileChannelConfiguration {

    @Value("${output-folder-integration}")
    private String outputDirectory;

    @Autowired
    private  IncomingFileUploadGateway fileUploadGateway;

    @Bean
    public IntegrationFlow fileFlow()
    {

        return IntegrationFlows.from(fileReader(),
                spec -> spec.poller(Pollers.fixedDelay(2000)))
                .handle( (Message<?> message) -> {

                    File payload = (File) message.getPayload();
                    fileUploadGateway.uploadFile(payload);
                    payload.delete();

                })
                .get();

    }

    @Bean
    public AbstractMessageSource<File> fileReader()
    {
        FileReadingMessageSource source = new FileReadingMessageSource();
        source.setDirectory(Paths.get(outputDirectory).toFile());
        source.setAutoCreateDirectory(true);
        source.setFilter(new SimplePatternFileListFilter("*.fin"));
        return source;
    }



    @Bean
    public IntegrationFlow flow()
    {
        return flow -> flow.split()
                .publishSubscribeChannel(
                        subscription -> subscription.subscribe(fileFlow())
                );

    }
}
