package com.mindlabssolutions.mirtgs.seggrator.configuration;

import ch.qos.logback.classic.Logger;
import com.jcraft.jsch.ChannelSftp;
import com.mindlabssolutions.mirtgs.seggrator.services.SFTPMessageHandlerService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.filters.FileSystemPersistentAcceptOnceFileListFilter;
import org.springframework.integration.file.remote.session.CachingSessionFactory;
import org.springframework.integration.file.remote.session.SessionFactory;
import org.springframework.integration.jdbc.metadata.JdbcMetadataStore;
import org.springframework.integration.metadata.ConcurrentMetadataStore;
import org.springframework.integration.sftp.filters.SftpPersistentAcceptOnceFileListFilter;
import org.springframework.integration.sftp.filters.SftpSimplePatternFileListFilter;
import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizer;
import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizingMessageSource;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

import javax.sql.DataSource;
import java.io.File;

@Configuration
public class SFTPConfiguration
{
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());



    @Value(value = "${outgoing-host}")
    private String domesticSFTPHost;

    @Value(value = "${outgoing-username}")
    private String domesticSFTPUserName;

    @Value(value = "${outgoing-password}")
    private String domesticSFTPPassword;

    @Value(value = "${outgoing-path}")
    private String domesticSftpPath;


    @Autowired(required = true)
    private DataSource dataSource;


    @Autowired
    private SFTPMessageHandlerService sftpMessageHandlerService;



    @Bean
    public SessionFactory<ChannelSftp.LsEntry> sftpSessionFactory()
    {
        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory(true);
        factory.setHost(domesticSFTPHost);
        factory.setPort(22);
        factory.setUser(domesticSFTPUserName);
        factory.setPassword(domesticSFTPPassword);
        factory.setAllowUnknownKeys(true);
        return new CachingSessionFactory<>(factory);
    }

    @Bean
    public ConcurrentMetadataStore metadataStore() {
        return new JdbcMetadataStore(dataSource);
    }

    @Bean
    public SftpInboundFileSynchronizer sftpInboundFileSynchronizer()
    {
        SftpInboundFileSynchronizer fileSynchronizer = new SftpInboundFileSynchronizer(sftpSessionFactory());
        fileSynchronizer.setDeleteRemoteFiles(false);
        fileSynchronizer.setRemoteDirectory(domesticSftpPath);
        fileSynchronizer.setFilter(new SftpSimplePatternFileListFilter("*.fin"));
        SftpPersistentAcceptOnceFileListFilter sftpPersistentAcceptOnceFileListFilter = new SftpPersistentAcceptOnceFileListFilter(metadataStore(), "swift");
        fileSynchronizer.setFilter(sftpPersistentAcceptOnceFileListFilter);
        return fileSynchronizer;
    }

    @Bean
    @InboundChannelAdapter(channel = "sftpChannel",poller = @Poller(fixedDelay = "5000"))
    public MessageSource<File> sftpMessageSource()
    {
        SftpInboundFileSynchronizingMessageSource source = new SftpInboundFileSynchronizingMessageSource(sftpInboundFileSynchronizer());
        source.setLocalDirectory(new File("sftp-swift"));
        source.setAutoCreateLocalDirectory(true);
        SftpPersistentAcceptOnceFileListFilter sftpPersistentAcceptOnceFileListFilter = new SftpPersistentAcceptOnceFileListFilter(metadataStore(), "swift");
        source.setLocalFilter( new FileSystemPersistentAcceptOnceFileListFilter(metadataStore(),"swift"));
        return source;
    }


    @Bean
    @ServiceActivator(inputChannel = "sftpChannel")
    public MessageHandler handler()
    {
        return new MessageHandler()
        {
            @Override
            public void handleMessage(Message<?> message) throws MessagingException
            {
                File file =(File) message.getPayload();
                sftpMessageHandlerService.handle(file);
            }
        };
    }
}
