package com.mindlabssolutions.mirtgs.seggrator.configuration;

import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.converter.SimpleMessageConverter;

import javax.jms.ConnectionFactory;

@Configuration
@EnableJms
public class ActiveMQConfiguration 
{
	
	  @Bean
	    public ConnectionFactory connectionFactory()
	  {
	        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	        
	                return connectionFactory;
	    }
	  
	  
	  
	  @Bean
	  public JmsListenerContainerFactory jmsListenerContainerFactory() 
	  {
	      DefaultJmsListenerContainerFactory factory =
	              new DefaultJmsListenerContainerFactory();
	      factory.setConnectionFactory(connectionFactory());
	      return factory;
	  }
	 
	  
	  @Bean
	   public MessageConverter converter(){
	        return new SimpleMessageConverter();
	    }
	  @Bean
	    public JmsTemplate jmsTemplate(){
	        JmsTemplate template = new JmsTemplate();
	        template.setConnectionFactory(connectionFactory());
	        return template;
	    }
	  @Bean
	  public ConnectionFactory cachingConnectionFactory()
	  {
	        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
	        connectionFactory.setTargetConnectionFactory(connectionFactory());
	        connectionFactory.setSessionCacheSize(10);
	        return connectionFactory;
	   }
	    

}
