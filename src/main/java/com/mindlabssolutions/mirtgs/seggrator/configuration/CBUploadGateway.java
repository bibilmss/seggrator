package com.mindlabssolutions.mirtgs.seggrator.configuration;


import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import java.io.File;

@MessagingGateway
public interface CBUploadGateway
{

    @Gateway(requestChannel = "CrossBorderSWIFTChannel")
    public void uploadFile(File file);
}
