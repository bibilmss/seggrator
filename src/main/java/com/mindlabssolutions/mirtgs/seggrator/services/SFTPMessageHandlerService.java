package com.mindlabssolutions.mirtgs.seggrator.services;

import ch.qos.logback.classic.Logger;
import com.google.gson.Gson;
import com.mindlabssolutions.mirtgs.seggrator.constants.SeggragatorConstants;
import com.mindlabssolutions.mirtgs.seggrator.pojos.SFTPSwiftFile;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.StringWriter;

@Service
public class SFTPMessageHandlerService
{
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());

    @Value("${input-folder-integration}")
    private String inputDirectory;


    @Autowired
    private BICService bicService;

    @Autowired
    private CBUploadService cbUploadService;

    @Autowired
    private JmsTemplate jmsTemplate;


    private void processSupportedMessage(String content,String filename)
    {
        SFTPSwiftFile ftpSwiftFile = new SFTPSwiftFile();
        ftpSwiftFile.setContent(content);
        ftpSwiftFile.setFilename(filename);
        Gson gson = new Gson();
        String json = gson.toJson(ftpSwiftFile);

        jmsTemplate.convertAndSend(SeggragatorConstants.STP_SUPPORTED_MESSAGES_QUEUE,json);
    }

    private void processOpposedMessage(File file)
    {
        cbUploadService.upload(file);
    }

    public void handle(File file)
    {

        try
        {

            String filename = file.getName();
            logger.info("FTP Filename=>"+filename);
            FileReader fileReader = new FileReader(file);
            int i=0;
            StringWriter stw = new StringWriter();
            while((i=fileReader.read())!=-1)
            {
                stw.append((char)i);
            }
            fileReader.close();


//            logger.info("message=>"+stw.toString());
            AbstractMT mt = AbstractMT.parse(stw.toString());

            if(mt.isType(103)||mt.isType(202))
            {
                String sender = mt.getSender();
                String receiver = mt.getReceiver().substring(0,8);

//                logger.info("Sender=>{}",sender);
                logger.info("Reciever=>{}",receiver);

//                Boolean isSenderParticipantSupported = bicService.findByParticipantcodeAndActiveTrue(sender);
                Boolean isRecieverParticipantSupported = false;

                isRecieverParticipantSupported = bicService.findByParticipantcodeAndActiveTrue(receiver);
                logger.info("isRecieverParticipantSupported:{}",isRecieverParticipantSupported);


                if(isRecieverParticipantSupported )
                {
                    try {
                        FileOutputStream fileOutputStream = new FileOutputStream( inputDirectory+ file.getName());
                        fileOutputStream.write(stw.toString().getBytes());
                        fileOutputStream.close();


                    }catch (Exception e){
                        logger.error("",e);
                    }
                }
                else
                {
                    this.processOpposedMessage(file);
                }

            }
            else
                {
                this.processOpposedMessage(file);

            }

        }

        catch (Exception e)
        {

            logger.error("handle error",e);

        }

    }



}
