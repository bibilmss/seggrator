package com.mindlabssolutions.mirtgs.seggrator.services;

import com.mindlabssolutions.mirtgs.seggrator.dao.BICRepository;
import com.mindlabssolutions.mirtgs.seggrator.dto.BICDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BICService
{


    @Autowired
    private BICRepository bicRepository;


    public Boolean  findByParticipantcodeAndActiveTrue(String participantCode)
    {
        String participantCodeSubStr = participantCode.substring(0, 8);


        List<BICDTO> byMxCodeAndActiveTrue = bicRepository.findByMxCodeAndActiveTrue(participantCodeSubStr);


        if(byMxCodeAndActiveTrue.size()>0)
            return true;
        else
            return false;




    }





}
