package com.mindlabssolutions.mirtgs.seggrator.services;

import ch.qos.logback.classic.Logger;
import com.mindlabssolutions.mirtgs.seggrator.configuration.CBUploadGateway;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class CBUploadService
{
    private Logger logger = (Logger) LoggerFactory.getLogger(this.getClass());




    @Autowired
    private CBUploadGateway cbUploadGateway;


    public void upload(File file)
    {
        try
        {
            cbUploadGateway.uploadFile(file);
//            Files.delete(Paths.get(path+file.getName()));
        }
        catch (Exception e)
        {
            logger.error("",e);

        }
    }
}
